package com.devcamp.task55_30.randomnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomnumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomnumberApplication.class, args);
		//khai báo obj class RandomNumber
		RandomNumber randomNumber = new RandomNumber();
		//In số random kiểu double từ 1-100 ra console
		System.out.println(randomNumber.randomDoubleNumber());
		//In số random kiểu int từ 1-10 ra console
		System.out.println(randomNumber.randomIntNumber());
	}

}
